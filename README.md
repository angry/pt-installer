# pt-installer

Helper Script for installing Packet-Tracer on a Non-Deb-System. 

## Initial Idea:
Idea and commands from [https://grupp-web.de](https://grupp-web.de/cms/2021/03/14/installation-of-cisco-packet-tracer-8-0-0-on-rpm-based-linux-systems/) and further posts on this site. I just merged the whole thing into one script.

## Required Packages:
The programm `ar` from the package `binutils` is needed.

## Usage:
* Download this Script or clone the repo to your computer
* Make the Script executable: `chmod +x pt8-installer.sh`
* Get PacketTracer8-Installation-File from your https://netacad.com account
* run this skript as root with the path to the PT-File. e. g.:  
  `sudo ./pt8-installer.sh Packet_Tracer822_amd64_signed.deb`

The script is tested on the following distributions:

* Linux Mint Debian Edition (LMDE 6) -> january 2025
* Debian 11 (KDE) -> june 2022
* openSUSE Tumbleweed (KDE) -> december 2021
* Manjaro Linux (KDE) -> december 2021
* Arch Linux (KDE) -> march 2021
* Fedora 33 (Gnome) -> march 2021

## Note:
On Arch-Linux or Arch-Derivates (Manjaro, ArcoLinux, ...) you can also install packettracer from the AUR: https://aur.archlinux.org/packages/packettracer/
